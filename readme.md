# Tangible Loops for EDD - Site views

This is a child theme installed on [the Tangible Loops for EDD site](https://loop.tangible.one/extend/easy-digital-downloads).

It contains templates for the site frontend, which is a documentation (and some testing) of plugin features.

It depends on a parent theme called [Tangible Views](https://bitbucket.org/tangibleinc/tangible-views).
